<?php
/**
 * Template Name: Product
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>

<div class="container product-slider">
	<div class="row row500">
		<div class="col-md-6">
			<h1>Sports Insider subscription direct to your phone</h1>
			<div class="fall-promo">
				<span class="old-price">$9.99</span>
				<span class="new-price">Free: 2019 Promotion</span>
			</div>
			<ul>
				<li>NHL, NFL & NBA point spread game picks sent to your phone daily</li>
				<li>Picks chosen by established sports analysts</li>
				<!-- <li>Just $9.99 / month</li> -->
			</ul>
			<a href="/checkout" class="btn btn-default">Free sign up</a>
			<span class="no-card">No Credit Card Required!</span>
		</div>
		<div class="col-md-6 img">
			<img src="/wp-content/uploads/2017/08/ios-phone-screenshot.png" alt="iPhone preview">
		</div>
	</div>
</div>

<div class="container-fluid testimonials">
	<div class="row">
		<div class="col-md-12">
			<h2>Our Proven Results</h2>
		</div>
		<!-- Revise these -->
		<!-- Revise these -->
		<div class="col-md-4 testimonial">
			<div class="row testimonialContent">
				<div class="col-md-12 review">
					<p>I Absolutely love this subscription. Since I've subscribed I've been able to make over 1000$ just on NHL bets alone. This year im anticipating on making even more!!</p>
				</div>
				<div class="col-lg-4 col-md-6 portrait">
					<img src="/wp-content/uploads/2017/08/kyle.jpg" alt="Samson">
				</div>
				<div class="col-lg-8 col-md-12 info">
					<p class="name">- Kyle</p>
					<p class="city">Ithaca, New York</p>
					</div>
			</div>
		</div>
		<!-- Revise these -->
		<div class="col-md-4 testimonial">
			<div class="row testimonialContent">
				<div class="col-md-12 review">
					<p>I like that it's only $10/month and convenient to be sent correct picks to my phone daily.</p>
				</div>
				<div class="col-lg-4 col-md-6 portrait">
					<img src="/wp-content/uploads/2017/08/brandon.jpg" alt="Brandon">
				</div>
				<div class="col-lg-8 col-md-12 info">
					<p class="name">- Brandon</p>
					<p class="city">Toronto, Ontario</p>
				</div>
			</div>
		</div>
		<!--  -->
		<div class="col-md-4 testimonial">
			<div class="row testimonialContent">
				<div class="col-md-12 review">
					<p>It's a pretty cool site because they highlight which games will make you your money back so I'm pretty much gaining free & correct knowledge that I share with my friends.</p>
				</div>
				<div class="col-lg-4 col-md-6 portrait">
					<img src="/wp-content/uploads/2017/07/samson.jpg" alt="Samson">
				</div>
				<div class="col-lg-8 col-md-12 info">
					<p class="name">- Samson</p>
					<p class="city">Montreal, Quebec</p>
					</div>
			</div>
		</div>

		<!--  -->
		<div class="col-md-12">
			<a href="/make-your-money-back/" class="btn btn-default">Learn how you will make your money back</a>
		</div>
		<!--  -->
	</div>
</div>

<?php

get_footer();

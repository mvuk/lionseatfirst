<?php
/**
 * Template Name: Terms
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>

<div class="container-fluid about mission about-intro-pgh">
	<div class="row rowCenter">
		<div class="col-md-12">
			<h1>Terms & Conditions</h1>
		</div>
		<div class="col-md-12">
			<h2>Conditions of use</h2>
			<p>Lions Eat First Sports & Entertainment provides their services to you subject to the following conditions. If you visit our website or use or services, you accept these conditions. Please read them carefully. </p>
		</div>
		<div class="col-md-12">
			<h2>Privacy</h2>
			<p>Please review our Privacy Notice, which also governs your visit to our website, to understand our practices.</p>
		</div>
		<div class="col-md-12">
			<h2>Electronic communications</h2>
			<p>When you visit Lions Eat First Sports & Entertainment or send e-mails to us, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices on this site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.</p>
		</div>
		<div class="col-md-12">
			<h2>Copyright</h2>
			<p>All content included on this site, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations, and software, is the property of Lions Eat First Sports & Entertainment or its content suppliers and protected by international copyright laws. The compilation of all content on this site is the exclusive property of Lions Eat First Sports & Entertainment, with copyright authorship for this collection by Lions Eat First Sports & Entertainment, and protected by international copyright laws.</p>
		</div>
		<div class="col-md-12">
			<h2>Trademarks</h2>
			<p>Lions Eat First Sports & Entertainment trademarks and trade dress may not be used in connection with any product or service that is not Lions Eat First Sports & Entertainment, in any manner that is likely to cause confusion among customers, or in any manner that disparages or discredits Lions Eat First Sports & Entertainment. All other trademarks not owned by Lions Eat First Sports & Entertainment or its subsidiaries that appear on this site are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by Lions Eat First Sports & Entertainment or its subsidiaries.</p>
		</div>
		<div class="col-md-12">
			<h2>License and site access</h2>
			<p>Lions Eat First Sports & Entertainment grants you a limited license to access and make personal use of this site and not to download (other than page caching) or modify it, or any portion of it, except with express written consent of Lions Eat First Sports & Entertainment. This license does not include any resale or commercial use of this site or its contents: any collection and use of any product listings, descriptions, or prices: any derivative use of this site or its contents: any downloading or copying of account information for the benefit of another merchant: or any use of data mining, robots, or similar data gathering and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of Lions Eat First Sports & Entertainment. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of Lions Eat First Sports & Entertainment and our associates without express written consent. You may not use any meta tags or any other "hidden text" utilizing Lions Eat First Sports & Entertainment name or trademarks without the express written consent of Lions Eat First Sports & Entertainment. Any unauthorized use terminates the permission or license granted by Lions Eat First Sports & Entertainment. You are granted a limited, revocable, and nonexclusive right to create a hyperlink to the home page of Lions Eat First Sports & Entertainment so long as the link does not portray Lions Eat First Sports & Entertainment, its associates, or their products or services in a false, misleading, derogatory, or otherwise offensive matter. You may not use any Lions Eat First Sports & Entertainment logo or other proprietary graphic or trademark as part of the link without express written permission.
			</p>
		</div>
		<div class="col-md-12">
			<h2>Your membership account</h2>
			<p>If you use this service, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. If you are under 19, you may use our website only with involvement of a parent or guardian. Lions Eat First Sports & Entertainment and its associates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion.</p>
		</div>
		<div class="col-md-12">
			<h2>Reviews, comments, emails and other content</h2>
			<p>Visitors may post reviews, comments, and other content: and submit suggestions, ideas, comments, questions, or other information, so long as the content is not illegal, obscene, threatening, defamatory, invasive of privacy, infringing of intellectual property rights, or otherwise injurious to third parties or objectionable and does not consist of or contain software viruses, political campaigning, commercial solicitation, chain letters, mass mailings, or any form of "spam." You may not use a false e-mail address, impersonate any person or entity, or otherwise mislead as to the origin of a card or other content. Lions Eat First Sports & Entertainment reserves the right (but not the obligation) to remove or edit such content, but does not regularly review posted content. If you do post content or submit material, and unless we indicate otherwise, you grant Lions Eat First Sports & Entertainment and its associates a nonexclusive, royalty-free, perpetual, irrevocable, and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, and display such content throughout the world in any media. You grant Lions Eat First Sports & Entertainment and its associates and sublicensees the right to use the name that you submit in connection with such content, if they choose. You represent and warrant that you own or otherwise control all of the rights to the content that you post: that the content is accurate: that use of the content you supply does not violate this policy and will not cause injury to any person or entity: and that you will indemnify Lions Eat First Sports & Entertainment or its associates for all claims resulting from content you supply. Lions Eat First Sports & Entertainment has the right but not the obligation to monitor and edit or remove any activity or content. Lions Eat First Sports & Entertainment takes no responsibility and assumes no liability for any content posted by you or any third party.
			</p>
		</div>
		<div class="col-md-12">
			<h2>Service descriptions</h2>
			<p>Lions Eat First Sports & Entertainment and its associates attempt to be as accurate as possible. However, Lions Eat First Sports & Entertainment does not warrant that service descriptions or other data is fully accurate, complete, reliable, current, or error-free. If the services offered by Lions Eat First Sports & Entertainment itself is not as described, your sole remedy is to contact us for a refund.</p>
		</div>
		<div class="col-md-12">
			<h2>Applicable Law</h2>
			<p>By visiting Lions Eat First Sports & Entertainment, you agree that the laws of the province of Ontario, Canada, without regard to principles of conflict of laws, will govern these Conditions of Use and any dispute of any sort that might arise between you and Lions Eat First Sports & Entertainment.</p>
		</div>
		<div class="col-md-12">
			<h2>Disputes</h2>
			<p>Any dispute relating in any way to your visit to Lions Eat First Sports & Entertainment or to services you purchase through Lions Eat First Sports & Entertainment shall be submitted to confidential arbitration in Ontario, Canada, except that, to the extent you have in any manner violated or threatened to violate Lions Eat First Sports & Entertainment intellectual property rights, Lions Eat First Sports & Entertainment may seek injunctive or other appropriate relief in any province or federal court in the state of Ontario, Canada, and you consent to exclusive jurisdiction and venue in such courts. Arbitration under this agreement shall be conducted under the rules then prevailing of the Canadian Arbitration Association. The arbitrators award shall be binding and may be entered as a judgment in any court of competent jurisdiction. To the fullest extent permitted by applicable law, no arbitration under this Agreement shall be joined to an arbitration involving any other party subject to this Agreement, whether through class arbitration proceedings or otherwise.
			</p>
		</div>
		<div class="col-md-12">
			<h2>Questions</h2>
			<p>Questions regarding our Conditions of Usage, Privacy Policy, or other policy related material can be directed to our support staff by clicking on the "Contact Us" link. Alternatively, you can email us at lefse@lefsent.com.</p>
		</div>
	</div>
	<!-- Photoshop a full lion here -->
</div>

<?php

get_footer();

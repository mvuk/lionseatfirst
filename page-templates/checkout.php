<?php
/**
 * Template Name: Checkout
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>

<!-- start content -->

<div class="container-fluid" id="checkoutHeader">
	<div class="row rowMax1140">
		<div class="col-md-12">
			<div class="imgHeader">
				<img src="/wp-content/uploads/2017/08/lions-eat-first-logo-text.png" alt="Lions Eat First">
			</div>
		</div>
	</div>
</div>

<div class="container-fluid" id="checkoutForm">
	<div class="row rowMax1140">
		<div class="col-md-12">
			<h2>Sports Insider Subscription</h2>
			<div class="fall-promo">
				<span class="old-price">$9.99</span>
				<span class="new-price">Free: 2019 Promotion</span>
			</div>
			<ul>
				<li>NHL, NBA and NFL game picks sent daily</li>
				<li>Picks chosen by established sports analysts</li>
				<li>Sent directly to your phone</li>
				<!-- <li>$9.99 / month</li> -->
			</ul>
		</div>
		<div class="col-md-12">
			<!--  -->
			<?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
			<!--  -->
		</div>
	</div>
</div>

<!-- end content -->

<?php

get_footer();

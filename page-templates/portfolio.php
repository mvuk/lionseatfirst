<?php
/**
 * Template Name: Portfolio / Make your money back
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>

<div class="container-fluid makemoneyback about-intro-pgh">
	<div class="row rowCenter">
		<div class="col-md-12">
			<h1>Make your money back</h1>
		</div>
		<div class="col-md-9">
			<p><strong>Our objective is simple - provide clients with accurate picks.</strong> We at LEFSE aim for a long term approach to sports wagering by promoting a strategic method that ensures our clients are going to make back their $10 investment and also maintain a positive in profit margins. We are not 100% accurate so we stress our clients to understand sport philosophy and practice correct money management.</p>
			<!-- Phrase it as ... we cannot garuntee wins/ "sporting event outcomes" -->
			<p>When subscribed to our monthly service we highlight games that we think will win you your money back. Whether you're a heads up bettor or a classic bettor betting on multiple games via point spread we have you covered.</p>
		</div>
	</div>
	</div>
</div>

<div class="container-fluid about-bar portfolioBar">
	<div class="row rowCenter">
		<div class="col-md-6 imgStat">
			<img src="/wp-content/uploads/2017/07/nba-score-box.png" class="raptorsGame">
		</div>
		<div class="col-md-6 text-bar">
			<p>Our data management platform allows us to compile stats across the entire NHL, NFL and NBA.</p>
		</div>
	</div>
</div>

<div class="container-fluid home-slider">
	<div class="row row500">
		<div class="col-md-6">
			<h1>Sports Insider subscription direct to your phone</h1>
			<ul>
				<li>NHL, NFL & NBA point spread game picks sent to your phone daily</li>
				<li>Picks chosen by established sports analysts</li>
			</ul>
			<a href="/checkout" class="btn btn-default">Sign up now</a>
		</div>
		<div class="col-md-6 img">
			<img src="/wp-content/uploads/2017/08/ios-phone-screenshot.png" alt="iPhone preview">
		</div>
	</div>
</div>
<?php

get_footer();

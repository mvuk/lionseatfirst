<?php
/**
 * Template Name: Who We Are
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>

<div class="container-fluid about mission about-intro-pgh">
	<div class="row rowCenter">
		<div class="col-md-12">
			<h1>Who we are</h1>
		</div>
		<div class="col-md-9">
			<p>Lions Eat First Sports & Entertainment is a sports betting consulting company that strives on a daily basis to provide its members with accurate picks. Our goal is to best guide our members' bets while implementing our core values such as:</p>
			<ul>
				<li><strong>Success - </strong><em>Providing</em> each member opportunities to earn as much possible.</li>
				<li><strong>Quality - </strong><em>Offering</em> superior service and value to its members.</li>
				<li><strong>Transparency - </strong><em>Delivering</em> honesty, integrity & professionalism with each pick.</li>
				<li><strong>Commitment - </strong><em>Conducting</em> research on data-driven platforms to guide our picks.</li>
				<li><strong>Loyalty - </strong><em>Building</em> a level of trust with each client through consistent results.</li>
			</ul>
		</div>
		<div class="col-md-9">
			<h2>Accurate bets through data-driven research</h2>
			<p>At Lions Eat First Sports & Entertainment we make it our mission to deliver to each member the best picks possible. We hope to enable all individuals to make more precise and knowledgeable sports bets, in the aim of increasing their profits. Our consistent research and analysis throughout each season will aid in making sure our data is as close to 100% as possible.</p>
		</div>
		<div class="col-md-9">
			<h2>Our vision</h2>
			<p>We aim to be leaders in sports betting consulting, helps all users to make the most profit possible. Our dedication to data, analytics and research will lead us to the most accurate picks possible.</p>
		</div>
	</div>
	<!-- Photoshop a full lion here -->
</div>

<div class="container-fluid about-bar whowearebar">
	<div class="row rowCenter">
		<div class="col-md-6 imgStat">
			<img src="/wp-content/uploads/2017/07/nba-summer-league-stats.png" class="nbaSummerStats">
		</div>
		<div class="col-md-6 text-bar">
			<p>Our passion for sports brings us into the depths of sports analytics research.</p>
			<a href="/make-your-money-back/" class="btn btn-default">Learn how you will make your money back</a>
		</div>
	</div>
</div>

<div class="container-fluid home-slider">
	<div class="row row500">
		<div class="col-md-6">
			<h1>Sports Insider subscription direct to your phone</h1>
			<ul>
				<li>NHL NFL & NBA point spread game picks sent to your phone daily</li>
				<li>Picks chosen by established sports analysts</li>
			</ul>
			<a href="/checkout" class="btn btn-default" >Sign up now</a>
		</div>
		<div class="col-md-6 img">
			<p class="summer-promo"><strike>$9.99</strike> Free! - Summer Promo</p>
			<img src="/wp-content/uploads/2017/08/ios-phone-screenshot.png" alt="iPhone preview">
		</div>
	</div>
</div>

<?php

get_footer();

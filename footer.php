<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>


<!-- BEGIN MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" id="modal-edit" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Lions Eat First Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
				<div class="row promo">
					<div class="col-md-12">
						<p class="summer-promo"><strike>$9.99</strike> Free - Summer Promo</p>
            <ul>
      				<li>NHL, NBA and NFL game picks sent daily</li>
      				<li>Picks chosen by established sports analysts</li>
      			</ul>
						<hr>
					</div>
				</div>
				<div class="row form">
					<!--  -->
          <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
					<!--  -->
				</div>
				<div class="row ssl">
					<div class="col-md-12">
						<p><i class="fa fa-lock" aria-hidden="true"></i>LEF servers are SSL Secure</p>
					</div>
				</div>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL  -->

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_html( $container ); ?>">

		<div class="row">

			<div class="col-md-4 contact-col">
				<h3>Contact</h3>
				<p><i class="fa fa-phone" aria-hidden="true"></i>647-863-9929</p>
				<p><i class="fa fa-envelope" aria-hidden="true"></i>lefse@lefsent.com</p>
        <p><i class="fa fa-globe" aria-hidden="true"></i>36 King St E, Toronto ON</p>
			</div>

			<div class="col-md-4 contact-col">
				<h3>Connect</h3>
				<a href="https://www.facebook.com/1LEFSE/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i>Facebook</a>
				<a href="https://www.instagram.com/lefse_/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</a>
			</div>

			<div class="col-md-4">
			</div>

			<div class="col-md-12">
				<footer class="site-footer" id="colophon">
					<div class="site-info">
						<p>Copyright 2017 Lions Eat First Sports & Entertainment</p>
            <div class="info-links">
              <a href="/terms-and-conditions" target="_blank">Terms & Conditions</a>
              <a href="/privacy-policy" target="_blank">Privacy Policy</a>
            </div>
					</div><!-- .site-info -->
				</footer><!-- #colophon -->
			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

<!-- EXTERNAL LINKS GO HERE -->

<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">

<!-- Google analytics -->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-105320839-1', 'auto');
ga('send', 'pageview');

</script>

<!--  -->

</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
